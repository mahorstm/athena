#!/bin/bash
# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
# Author : Benjamin Trocme (CNRS/IN2P3 - LPSC Grenoble) - 2017 - 2023
#
# Daemon job to update daily the year stats
# 
# Update of the run with ATLAS ready, production of the weekly reports and
# update of year stats
# Arguments:
# -$1 : directory when to run the daemon (a priori ~atlasdqm/w1/DeMo)
# -$2 : DeMo year
# -$3 : DeMo tag (a priori Tier0_[year])
##################################################################

rundir=$1
year=$2
tag=$3

if [ "$#" == "4" ];
then
    option=$4
else
    option="NoOption"
fi

date=`date`
echo "Date is ${date}"
cd ${rundir}
echo "Now in ${rundir}"
export AtlasSetup=/afs/cern.ch/atlas/software/dist/AtlasSetup
source $AtlasSetup/scripts/asetup.sh --stable 24.0,Athena,latest
export ScriptPath=$rundir

echo "First, looking for new runs..."
cmd="python3 $ScriptPath/DeMoUpdate.py --runListUpdate -y ${year}"
echo "** ${cmd} **"
eval "${cmd}"

systems='Pixel SCT TRT LAr Tile MDT TGC RPC Trig_L1 Trig_HLT Lumi Global ALFA AFP LUCID ZDC IDGlobal BTag CaloCP MuonCP '

if [ $option == "resetYS" ];
then
    echo "Warning : I am reseting the year stats"
else
    echo "I am updating the year stats"
fi

for system in $systems
do
    echo "====================================================================================="
    echo "====================================================================================="
    echo "Processing "${system}
    echo "====================================================================================="
    echo "====================================================================================="
    
    # Script for the weekly production
    cmd="python3 $ScriptPath/DeMoUpdate.py -b -y ${year} -t ${tag} -s ${system} --weeklyReport --onlineLumiNorm --vetoLumiEvol"
    log="YearStats-${system}/${year}/${tag}/daemon-weekly.out"
    echo "** ${cmd} **"
    eval "rm -f ${log}"
    eval "${cmd} &> ${log}"


    # Script for updating the year stats
    if [ $option == "resetYS" ];
    then
	cmd="python3 $ScriptPath/DeMoUpdate.py -b -y ${year} -t ${tag} -s ${system} --resetYearStats --updateYearStats"
    else
	cmd="python3 $ScriptPath/DeMoUpdate.py -b -y ${year} -t ${tag} -s ${system} --skipAlreadyUpdated --updateYearStats"
    fi
    log="YearStats-${system}/${year}/${tag}/daemon-grl.out"
    echo "** ${cmd} **"
    eval "rm -f ${log}"
    eval "${cmd} &> ${log}"

    # Display of the year stats plots
    cmd="python3 $ScriptPath/DeMoStatus.py -b -s ${system} -y ${year} -t ${tag} --yearStats --savePlots"
    log="YearStats-${system}/${year}/${tag}/daemon-YSplots.out"
    echo "** ${cmd} **"
    eval "rm -f ${log}"
    eval "${cmd} &> ${log}"

    # Producing the defect recap
    cmd="python3 $ScriptPath/DeMoScan.py -b -y ${year} -t ${tag} -s ${system} --recapDefects"
    log="YearStats-${system}/${year}/${tag}/daemon-recapDefects.out"
    echo "** ${cmd} **"
    eval "rm -f ${log}"
    eval "${cmd} &> ${log}"
    
done

echo "Derive ATLAS data loss"
cmd="python3 DeMoAtlasDataLoss.py -b -y ${year} -t ${tag}"
eval ${cmd}

echo "Generate webpage"
cmd="python3 $ScriptPath/DeMoGenerateWWW.py -y ${year}"
eval ${cmd}
