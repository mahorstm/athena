#!/bin/bash
# art-description: Run 4 configuration, ITK only recontruction with ACTS, electron events with pt=10GeV
# art-type: grid
# art-include: main/Athena
# art-output: *.root
# art-output: *.xml
# art-output: dcube*
# art-html: dcube_acts_shifter_last

rdo=/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/PhaseIIUpgrade/RDO/ATLAS-P2-RUN4-03-00-00/mc21_14TeV.900494.PG_single_epm_Pt10_etaFlatnp0_43.recon.RDO.e8481_s4149_r14697/RDO.33628973._000032.pool.root.1

script=test_run4_acts_ckf_epm_reco.sh
echo "Executing script ${script} "
bash ${script} ${rdo} 
